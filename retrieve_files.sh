#!/bin/bash
set -euo pipefail

kent_tools=$HOME/bin/ucsc_tools

if [[ ! -x ${kent_tools}/fetchChromSizes || ! -x ${kent_tools}/wigToBigWig ]]
then
    echo Please modify the kent_tools variable in this script to point to a directory containing the fetchChromSizes and wigToBigWig executables from http://hgdownload.soe.ucsc.edu/admin/exe/
    exit 1
fi

echo $(date) Retrieving top1-seq wig files from GEO
geo_dir=GSE57628
mkdir -p $geo_dir

wget 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM1385718&format=file&file=GSM1385718%5FHCT116%5Fwt%5F%2BCPT%5FTop1%5FSeq%5Frep2%5Fallreads%2Ewig%2Egz' -O ${geo_dir}/GSM1385718_HCT116_wt_+CPT_Top1_Seq_rep2_allreads.wig.gz 

wget 'https://www.ncbi.nlm.nih.gov/geo/download/?acc=GSM1385717&format=file&file=GSM1385717%5FHCT116%5Fwt%5F%2BCPT%5FTop1%5FSeq%5Frep1%5Fallreads%2Ewig%2Egz' -O ${geo_dir}/GSM1385717_HCT116_wt_+CPT_Top1_Seq_rep1_allreads.wig.gz 

echo $(date) Converting wigs to bigwigs
mkdir -p ref
${kent_tools}/fetchChromSizes hg19 > ref/hg19.chrom.sizes

${kent_tools}/wigToBigWig GSE57628/GSM1385717_HCT116_wt_+CPT_Top1_Seq_rep1_allreads.wig.gz ref/hg19.chrom.sizes GSE57628/GSM1385717_HCT116_wt_+CPT_Top1_Seq_rep1_allreads.bw

${kent_tools}/wigToBigWig GSE57628/GSM1385718_HCT116_wt_+CPT_Top1_Seq_rep2_allreads.wig.gz ref/hg19.chrom.sizes GSE57628/GSM1385718_HCT116_wt_+CPT_Top1_Seq_rep2_allreads.bw

echo $(date) Retrieving PCAWG consensus variant calls from ICGC
mkdir -p pcawg
wget 'https://dcc.icgc.org/api/v1/download?fn=/PCAWG/consensus_snv_indel/final_consensus_passonly.snv_mnv_indel.icgc.public.maf.gz' -O pcawg/final_consensus_passonly.snv_mnv_indel.icgc.public.maf.gz
wget 'https://dcc.icgc.org/api/v1/download?fn=/PCAWG/mutational_signatures/Signatures_in_Samples/SP_Signatures_in_Samples/PCAWG_SigProfiler_ID_signatures_in_samples.csv' -O pcawg/PCAWG_SigProfiler_ID_signatures_in_samples.csv
wget 'https://dcc.icgc.org/api/v1/download?fn=/PCAWG/donors_and_biospecimens/pcawg_sample_sheet.tsv' -O pcawg/pcawg_sample_sheet.tsv


echo $(date) Retrieving Umap hg19/k36 mappability data
mkdir -p umap
umap_bed=umap/k36.umap.bed.gz
wget 'https://bismap.hoffmanlab.org/raw/hg19/k36.umap.bed.gz' -O $umap_bed

echo $(date) Removing track header from bed
zgrep -v 'track name' $umap_bed | bgzip -c > umap/hg19.k36.umap.bed.gz

echo $(date) Retrieving reference genomes
wget http://hgdownload.cse.ucsc.edu/goldenpath/hg19/bigZips/hg19.fa.gz -O ref/hg19.fa.gz
gzip -d ref/hg19.fa.gz

wget ftp://ftp-trace.ncbi.nih.gov/1000genomes/ftp/technical/reference/phase2_reference_assembly_sequence/hs37d5.fa.gz -O ref/hs37d5.fa.gz
gzip -d ref/hs37d5.fa.gz || true

echo $(date) Retrieving Gencode gene annotations
mkdir -p gencode
wget 'http://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_38/GRCh37_mapping/gencode.v38lift37.annotation.gff3.gz' -O gencode/gencode.v38lift37.annotation.gff3.gz
echo $(date) Converting to bed of transcript regions
zcat gencode/gencode.v38lift37.annotation.gff3.gz | \
    perl -wanE 'next if /^#/; if ($F[2] eq "transcript"){ my @info = split(";", $F[8]); ($name = $info[0]) =~ s/^ID=//; say join("\t", $F[0], $F[3] - 1, $F[4], $name, 0, $F[6]);}' | \
    bedtools sort | \
    bgzip -c > gencode/gencode.v38lift37.annotation.transcripts.bed.gz

echo $(date) Done
