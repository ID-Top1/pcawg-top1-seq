# PCAWG TOP1-seq

Analysis of deletions in PCAWG ID4-positive tumours relative to TOP1-seq signal from Baranello et al. (2016): doi:10.1016/j.cell.2016.02.036


## INSTALLATION/USAGE

### SYNOPSIS

~~~
$ git clone --recursive https://git.ecdf.ed.ac.uk/Deletions_paper/pcawg-top1-seq.git
$ cd pcawg-top1-seq
$ ./retrieve_files.sh
$ jupyter-notebook
~~~

### Software/Modules

To run these notebooks you will need [jupyter](https://jupyter.org/) (either
jupyter notebook or jupyter lab) installed with a python3 kernel. The following
python modules are also required:

* pyBigWig
* pyfaidx
* pandas
* numpy
* scipy
* scikit-learn
* matplotlib
* seaborn
* indel_repeat_classifier (https://github.com/david-a-parry/indel_repeat_classifier)
* region_finder (https://github.com/david-a-parry/region_finder)

These can all be installed via pip, as per the example below:

~~~
# install all those available via PyPi
$ python3 -m pip install pyBigWig pyfaidx pandas numpy scikit-learn scipy matplotlib seaborn --user

# install those available via github
$ python3 -m pip install git+git://github.com/david-a-parry/indel_repeat_classifier.git --user
$ python3 -m pip install git+git://github.com/david-a-parry/region_finder.git --user
~~~


### DATA

The `retrieve_files.sh` bash script will automatically download the publically available variant data and reference files required to run these analyses. You will need to edit line 4 of this script to specify the location of `fetchChromSizes` and `wigToBigWig` executables on your system. The versions of these exectutables for your system can be downloaded from http://hgdownload.soe.ucsc.edu/admin/exe/.

Once you have run:

    $ ./retrieve_files.sh

...and this has completed without errors you are ready to go with the jupyter notebooks.

Note that `01-top1seq_bigwig_to_dataframe.ipynb` and `02-pcawg_variant_processing.ipynb` can be run concurrently but `03-top1seq_vs_pcawg.ipynb` requires both of the preceding notebooks to have been run to completion in order to generate the necessary data. These analyses can use up quite a lot of RAM so it is recommended that you shut down the kernel for each notebook when finished.


## AUTHOR

Written by David A. Parry at the University of Edinburgh.
